#include "requestprocessor.h"

#include "request.h"


RequestProcessor::RequestProcessor()
{
}

RequestProcessor::~RequestProcessor()
{
    stopper_.Stop();

    for (auto &t : recivers_)
    {
        t.join();
    }

    for (auto &t : processors_)
    {
        t.join();
    }

}

void RequestProcessor::Start(int reciversCount, int processorsCount)
{
    for (int i = 0; i < reciversCount; ++i)
    {
        recivers_.emplace_back(&RequestProcessor::reciverProc, this);
    }

    for (int i = 0; i < processorsCount; ++i)
    {
        recivers_.emplace_back(&RequestProcessor::processorProc, this);
    }
}

void RequestProcessor::reciverProc()
{
    while (!stopper_.IsStopped())
    {
        auto request = GetRequest(stopper_);

        if (request)
        {

            if (!stopper_.IsStopped())
            {
                std::lock_guard<std::mutex> lock(queueLock_);
                requestQueue_.emplace_back(request, [](Request *r){ DeleteRequest(r); });
            }
            else
                DeleteRequest(request);

        }
    }
}

void RequestProcessor::processorProc()
{
    while (!stopper_.IsStopped())
    {
        WrappedRequest request;
        {
            std::lock_guard<std::mutex> lock(queueLock_);

            if (!requestQueue_.empty())
            {
                request = std::move(requestQueue_.front());
                requestQueue_.pop_front();
            }
        }
        if (request)
        {
            ProcessRequest(request.get(), stopper_);
        }
    }
}
