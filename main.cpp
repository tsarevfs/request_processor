#include <iostream>
#include <chrono>
#include <thread>

#include "requestprocessor.h"

const int THREAD_COUNT = 3;

int main()
{
    RequestProcessor requestProcessor;
    requestProcessor.Start(THREAD_COUNT, THREAD_COUNT);

    std::this_thread::sleep_for(std::chrono::seconds(5));

    return 0;
}


