#include "request.h"
#include <iostream>


Request *GetRequest(Stopper stopSignal)
{
	//test implementation
    if (stopSignal.IsStopped())
    {
        return nullptr;
    }

    return new Request;
}

void ProcessRequest(Request *request, Stopper stopSignal)
{
	//test implementation
   std::cout << "request processed:" << std::endl;
}

void DeleteRequest(Request *request)
{
	//test implementation
    delete request;
}
