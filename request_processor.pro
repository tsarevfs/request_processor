TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    request.cpp \
    requestprocessor.cpp \
    stopper.cpp

HEADERS += \
    request.h \
    requestprocessor.h \
    stopper.h

QMAKE_CXXFLAGS += -std=c++11

LIBS += -lpthread
