#pragma once
#include <deque>

#include <vector>
#include <thread>
#include <mutex>

#include "stopper.h"

class Request;

class RequestProcessor
{
public:
	RequestProcessor();
    ~RequestProcessor();

    void Start(int reciversCount, int processorsCount);

private:
    void reciverProc();
    void processorProc();

private:
    using WrappedRequest = std::unique_ptr<Request, std::function<void(Request*)>>;

    std::deque<WrappedRequest> requestQueue_;
    std::mutex queueLock_;

    std::vector<std::thread> recivers_;
    std::vector<std::thread> processors_;

    Stopper stopper_;
};
