#include "stopper.h"

Stopper::Stopper()
    : stop_(std::make_shared<bool>(false))
{
}

void Stopper::Stop()
{
    *stop_ = true;
}

bool Stopper::IsStopped() const
{
    return *stop_;
}
