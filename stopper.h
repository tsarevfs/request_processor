#pragma once
#include <memory>

class Stopper
{
public:
    Stopper();

    void Stop();
    bool IsStopped() const;

private:
    std::shared_ptr<bool> stop_;
};
